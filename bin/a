#!/bin/sh

usage()
{
    echo "usage: ${0##*/} [start|volume|coc|screen] ARGS" 1>&2
    exit "$@"
}

warn()
{
    echo "${0##*/}: $*" 1>&2
}

err()
{
    warn "$@"
    exit 1
}

cmd_start()
{
    [ $# -ge 1 ] || err "missing start argument (try \`-l' to list applications)"

    case "$1" in
    coc)
        $PREFIX monkey -p com.supercell.clashofclans -c android.intent.category.LAUNCHER 1
        exit $?
        ;;
    tunein)
        $PREFIX monkey -p radiotime.player -c android.intent.category.LAUNCHER 1
        ;;
    -l)
        cat << EOF
coc
tunein
EOF
        exit 0
        ;;
    *)
        err "$1: invalid application"
        ;;
    esac
}

cmd_volume()
{
    [ $# -ge 1 ] || err "missing volume argument"

    case "$1" in
    up) shift; $PREFIX input keyevent "$@" 24; exit $? ;;
    down) shift; $PREFIX input keyevent "$@" 25; exit $? ;;
    -h)
        echo "usage: ${0##*/} volume [up|down]"
        exit 0
        ;;
    *)
        err "$1: invalid argument"
        ;;
    esac
}

cmd_coc()
{
    [ $# -ge 1 ] || err "missing coc argument"

    case "$1" in
    attack) adb shell input touchscreen tap 20 600; exit $?  ;;
    -h)
        echo "usage: ${0##*/} coc [attack]"
        exit 0
        ;;
    *)
        err "$1: invalid argument"
        ;;
    esac
}

is_screen_on()
{
    $PREFIX dumpsys power | grep -q mScreenOn=true
}

cmd_screen()
{
    [ $# -ge 1 ] || err "missing screen argument"

    case "$1" in
    on) is_screen_on || $PREFIX input keyevent 26; exit $? ;;
    off) is_screen_on && $PREFIX input keyevent 26; exit $? ;;
    -h)
        echo "usage: ${0##*/} screen [on|off]"
        exit 0
        ;;
    *)
        err "$1: invalid argument"
        ;;
    esac
}

main()
{
    [ $# -ge 1 ] || err "missing arguments"

    if [ -f /etc/lsb-release ]; then
        PREFIX="adb shell"
    else
        PREFIX=""
    fi

    case "$1" in
    start) shift; cmd_start "$@" ;;
    volume) shift; cmd_volume "$@" ;;
    coc) shift; cmd_coc "$@" ;;
    screen) shift; cmd_screen "$@" ;;
    -h) usage 0 2>&1 ;;
    *) warn "$1: invalid argument"; usage 1 ;;
    esac
}

main "$@"

# vim:set sw=4 ts=4 et:
